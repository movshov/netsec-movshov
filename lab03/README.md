Lab 3: Public Key Crypto
========================

In this lab, we get our first taste of public key crypto by implementing "toy" versions of some classical algorithms. 

WARNING: Not Safe for the Real World
------------------------------------
Please remember that, in this lab, we are building **simplified "toy" versions** of these constructions, solely for your **educational benefit**.  Much like you wouldn't risk your real data by running a toy operating system that you wrote in OS class, it's definitely not a good idea to build your own cryptographic tools for use against real adversaries.

In the real world, you can use several publicly available libraries like [libsodium](https://download.libsodium.org/doc/) or [libressl](https://www.libressl.org/), or even [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/). Many programming languages like Java also now include cryptographic implementations in their standard libraries.

Challenge Problems
------------------
* [Problem 0: Numeric decoding](prob00.md)
* [Problem 1: RSA Keygen (small keys)](prob01.md)
* [Problem 2: RSA Keygen (slightly larger keys)](prob02.md)
* [Problem 3: RSA Decryption](prob03.md)
* [Problem 4: RSA Signature Verification](prob04.md)
* [Problem 5: ElGamal Signature](prob05.md)
* [Problem 6: Diffie-Hellman Key Exchange](prob06.md)
