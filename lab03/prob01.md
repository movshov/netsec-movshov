Problem 1: RSA Keygen (small keys)
==================================

In this lab, you will be implementing a simple version of the RSA encryption and digital signature schemes. Your first step towards this goal will be to generate very small (and therefore insecure!) but valid RSA keypairs.

Connect to the server at 10.96.0.1 on port 3001, and it will tell you the desired length of your key (in bits). For example:
```console
$ telnet 10.96.0.1 3001
KEYLENGTH: 24
```

To proceed in the protocol, you must respond with the components of a **valid RSA key pair** of the desired length. Specifically, you must supply the server with:

* Two large primes ```p``` and ```q```.
  Note: BOTH ```p``` and ```q``` must be "large".  Here, that means if the requested key length is ```n```, then both ```p``` and ```q``` must be at least 2 to the power ```(n/2)-1```.

* The product of your two primes, ```N = p * q```.
  Note: ```N``` should be a "large" number of about ```n``` bits.  Here, ```N``` must be between 2 to the power ```n-1``` and 2 to the power ```n```.

* Exponents ```e``` and ```d``` for the private and public keys, respectively.
  Your private exponent ```e``` must be relatively prime with ```phi(N) = (p-1)*(q-1)```, and ```e``` and ```d``` must satisfy the equation ```e * d mod phi(N) == 1```.

Provide the server with the required values, one on each line, with a label to indicate which value is which. The server will verify that your numbers make up a valid RSA key pair. If successful, it will reply with "OK", and you can request the flag using our standard protocol.

For example, if you were really good at doing math quickly in your head (or if you precomputed the key), you could do this one interactively by typing the answers into Telnet, like this.  Lines that start with ```>``` come from the client (that is, from you).

```console
$ telnet 10.96.0.1 3001
KEYLENGTH: 16
> p: 139
> q: 359
> N: 49901
> e: 5
> d: 9881
OK
> GET FLAG
FLAG ...
```

**NOTE**: Remember to terminate each line with a carriage return ("\r") and a line feed ("\n").  The server expects these, and it may get confused if you don't include them.

Turn in your code ([prob01.py](prob01.py)) and flags ([flag01.txt](flag01.txt)) through Git, as in the previous labs. 
