Problem 4: RSA Signature Verification
=====================================

Next you'll use RSA to verify the signature on a flag. Connect to 10.96.0.1 on TCP port 3004 and request the flag using our standard protocol. The server's reply will be of the form
```
FLAG flag sig
```
where ```flag``` is the flag, formatted as a large integer, and ```sig``` is a naive "textbook" RSA signature on flag.

The server's public key consists of the modulus ```N=3110232614083941699686461``` and the public exponent ```e=3```.

***CAUTION!*** Like the hash verification and MAC verification problems in the [previous lab](../lab02/README.md), this server often returns fake flags and/or invalid signatures.  Only the correct flag will have a valid signature created with the server's public key.
