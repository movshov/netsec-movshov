Problem 2: RSA Keygen (slightly larger keys)
============================================

Now that you have your basic RSA key generation algorithm working, it's time to scale it up to handle larger key sizes. This problem is exactly like the [previous problem](prob01.md), but here the key lengths are longer. You'll probably need more efficient algorithms for testing primality and relative primality of your parameters, and for computing ```d``` as ```e``` 's multiplicative inverse.

A reasonably good way to generate large prime numbers is to simply generate lots of large numbers until you find one that is prime. You can use [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/)'s [Crypto.Random.random](https://pycryptodome.readthedocs.io/en/latest/src/random/random.html#crypto-random-random-module) module to generate the random numbers. Then you have many options for primality testing algorithms. There are exact algorithms that are slow, and there are probabilistic algorithms that are fast but may not detect when a number is composite (non-prime). So it makes sense to use two or more algorithms together in tandem. For example, if you find a number that passes several rounds of the [Fermat test](https://en.wikipedia.org/wiki/Fermat_primality_test), then it's probably a prime number, and it's probably worth your time to run the slower, but exact, [AKS test](https://en.wikipedia.org/wiki/AKS_primality_test) to know for sure that it's prime.

To find an exponent ```e``` that is relatively prime with your ```phi(N)```, you'll need an efficient algorithm for testing co-primality, like for example the [Euclidean algorithm](https://en.wikipedia.org/wiki/Euclidean_algorithm). Fortunately, Python provides this already in the [fractions](https://docs.python.org/3.7/library/fractions.html) package.

To compute ```d``` (```e```'s [modular multiplicative inverse](https://en.wikipedia.org/wiki/Modular_multiplicative_inverse)), you can easily write up a version of the [extended Euclidean algorithm](https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm).

The server is listening on 10.96.0.1, port 3002. Use the same protocol as in the [previous problem](prob01.md) to retrieve the flag.
