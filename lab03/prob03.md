Problem 3: RSA Decryption
=========================

Now that you can generate RSA keys, in this problem you will use them to decrypt the flag.

Connect to the server at 10.96.0.1 port 3003 and request the flag using a specialized version of our standard flag retrieval protocol. The format for your request shall be
```
GET FLAG N e
```
where ```N``` is your RSA modulus and ```e``` is your public key exponent.

The server first converts your flag into a large integer in the range (1,2^80). Then it encrypts the number using your public key and sends the ciphertext number to you. The format of the server's response message will be
```
FLAG c
```
where ```c``` is a large integer equal to ```m^e mod N``` and the number ```m``` is your flag.

Use your private key ```(N,d)``` to decrypt the flag and convert it back from an 80-bit integer into a string of 20 hexadecimal characters like most of our other flags.  (Just like you did in [Problem 0](prob00.md).)

Turn in your code ([prob03.py](prob03.py)) and flag ([flag03.txt](flag03.txt)) in our usual format.

**HINT**: You may know that Python supports exponentiation as a basic math operation. For example, ```2 ** 3 == 8```.  However, if you repeatedly multiply large numbers before performing the modulus operation, the size of your number will rapidly get out of hand, and your program will slow to a halt.  Instead, you can use the built-in function ```pow()```, which takes the modulus as an optional argument and uses a more efficient algorithm internally.  Click [here](https://docs.python.org/3/library/functions.html#pow) for documenation on ```pow()```.

