Problem 6: Diffie-Hellman Key Exchange
======================================

There is a server at 10.96.0.1 on port 3006. To retrieve its flag, you must first perform a Diffie-Hellman key exchange with the server to derive a shared secret key. You can then use the shared secret key to send an encrypted request ("GET FLAG") and decrypt the server's encrypted response (the plaintext will be "FLAG flag").

For a review of the Diffie-Hellman key exchange, see [Noah Dietrich's blog post](http://sublimerobots.com/2015/01/simple-diffie-hellman-example-python/). Here, our server uses the prime ```p = 999,959``` and the generator ```g = 2```.

**NOTE**: In order to make this exercise possible without importing additional libraries, we're using small numbers that are NOT SAFE for use in the real world. (Using ```g = 2``` is OK. In practice, ```p``` should be much larger.)

The protocol works as follows:

1. When you connect, the server immediately sends you its public key:
  ```
  PUBKEY pubkey
  ```
  where ```pubkey``` is a standard decimal-encoded integer. If the server's private key is ```s```, then the server's public key is equal to ```g^s mod p```.

2. You should reply with your public key in the same format:
  ```
  PUBKEY pubkey
  ```
  If your private key is ```c```, then you can compute your public key as ```g^c mod p```.

3. Both sides then derive the session key as ```MD5(g^(s*c) mod p)```.

4. You can now encrypt your request (```GET FLAG```) with AES-128 in CBC mode using the session key. If your request does not fill up the last CBC block, you should use [PKCS7 padding](http://tools.ietf.org/html/rfc5652#section-6.3) to fill the remaining bytes. Then send your encrypted request to the server as **raw binary bytes**.

5. The server attempts to decrypt your request, and if successful, it sends back a response (```FLAG flag```) encrypted with the same key in the same way.

Turn in your code ([prob06.py](prob06.py)) and flag ([flag06.txt](flag06.txt)) in git.

Question
--------
Q. What's wrong with this scheme? How many ways can you find to attack it?

Turn in your answer as [answer06.txt](answer06.txt) in git.

