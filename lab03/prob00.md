Problem 0: Numeric decoding
===========================
Here's an easy warm-up exercise to get you started. Connect to 10.96.0.1 on port 3000 and request the flag using our standard protocol. It will reply with your flag, written as an **80-bit decimal integer**.

Your job is to convert this large integer back into our normal flag format, which is a string of **20 hexadecimal characters**. We will use this functionality later with RSA.

Submit your code as [prob00.py](prob00.py) and your (converted, hexadecimal) flag as [flag00.txt](flag00.txt) in git.
