Problem 10: Hidden TCP Service with Challenge Response
======================================================
There is a server running on a high-numbered port at IP address 10.96.0.1. Find the open port and retrieve the flag.

**NOTE**: This server is running the same protocol as in [Problem 06](prob06.md), so you'll need to solve a math problem in order to get the flag.

Turn in your code in [prob10.py](prob10.py) and your flag in [flag10.txt](flag10.txt) in git.


Reminder: Don't forget to commit and push to git!
-------------------------------------------------
If you want to make sure all your changes are turned in, the following code snippet will do it:

```console
 git add prob*.py flag*.txt answer*.txt
 git commit -m "Maybe done with Lab 01"
 git push personal master
```
