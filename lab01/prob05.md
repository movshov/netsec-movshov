Problem 5: TCP Client with Password
===================================
Follow the example code at [python.org](https://wiki.python.org/moin/TcpCommunication) to write a TCP client. Connect to IP address 10.96.0.1 on TCP port 1105 and the server will ask you for the password. If you provide the correct password, you may demand the flag as in [Problem 4](prob04.md), and the server will send it to you.

**NOTE**: The password was unwisely chosen based on a dictionary word. The list of words in the dictionary can be found in [dictionary.txt](dictionary.txt) in git.

Turn in your code as [prob05.py](prob05.py) and your flag in [flag05.txt](flag05.txt) in git.

