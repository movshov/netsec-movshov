Problem 1: A Simple UDP Server
==============================

Follow the example code at [python.org](https://wiki.python.org/moin/UdpCommunication) to write a UDP server. Listen on your Wireguard interface (e.g. 10.96.x.y -- or on all interfaces, using the IP address 0.0.0.0), on UDP port 1101 for a packet containing the flag.


Turn in your code for problem 1 as [prob01.py](prob01.py) and the flag as [flag01.txt](flag01.txt) in git.


Question 1:
-----------
Q. How else could you have captured this flag, without using a socket?

Turn in your answer to this question in a new file called [answer01.txt](answer01.txt) in git. 


Turn in your Problem 1 files now!
=================================
Did you just complete Problem 1? If so, congratulations! You've captured your first flag.


Take a moment to celebrate by pushing your files to git using the steps we described [above](README.md).


To avoid late penalties, it's always best to push your code and flags as soon as you complete a problem. If you make improvements to your solution later, you can always push a new version at any time.
