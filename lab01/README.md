Network Security Lab 01: The TCP/IP Treasure Hunt
=============================================

Here we dust off our knowledge of TCP/IP network programming by writing some simple clients and servers, and we learn how to capture flags in the lab's virtual network environment.

Getting Started: Logging in to your VM
--------------------------------------

Your first order of business is to log in to your virtual machine. To do this, you will need the SSH private key that you created when we set up our VMs. You can find your VM's internet protocol (IP) address in the Google Compute Engine project for the course. Then, connect to your VM over SSH like this:

```console
 ssh YOUR_ODIN_ID@YOUR_VMS_IP
```

Because the VM server already has your public key, it will use this to identify you and log you in. You need the corresponding private key to prove to the server that you're really you. If you saved your private key in a file other than .ssh/id_rsa, then you may need to use the -i option to tell SSH where it can find your key. For example, if you saved your private key in netsec/private_key, then your SSH command would look like this: 

```console
 ssh -i netsec/private_key YOUR_ODIN_ID@YOUR_VMS_IP
```

Once you've logged in, the first order of business is to clone the git repository that contains the skeleton code for your lab assignments. The following command clones the repo into a new local directory called "labs":

```console
 git clone git@bitbucket.org:cvwpdx/netsec-public.git labs
```

You can also get a copy of this repo on your workstation in the lab, or on the Linux servers like Ada. For example:

```console
cvwright@ada:~$ git clone git@bitbucket.org:cvwpdx/netsec-public.git netsec-labs
```

Running the tests
-----------------
Each programming lab comes with a set of automated tests, so you can know when your code is working before you even turn it in.  You can run the tests like this:

```console
student@vm:~$ cd netsec-labs/lab01
student@vm:lab01$ pytest-3 --verbose tests.py
```

Turning in your lab work
------------------------
To turn in your flags and code for this lab, you will "push" to your personal Git respository. For example, to turn in flags and code for the first two problems:

```console
cvw@netsec-vm:lab01$ git add prob01.py flag01.txt answer01.txt prob02.py flag02.txt
```
Don't forget this step! Otherwise nothing gets committed.

```console
cvw@netsec-vm:lab01$ git commit -m "Got Problems 1 and 2 working. Yay!"
cvw@netsec-vm:lab01$ git push git@bitbucket.org:YOUR_BITBUCKET_USERNAME/netsec-YOUR_ODIN_ID.git master
```
For example, the URL for my "student" repo would be "git@bitbucket.org:cvwpdx/student-cvw.git". By default, this command pushes your changes to the "master" branch, which is where I'll pull from when I do the grading.

**NOTE**: At this point, Git will probably yell at you and demand that you provide it with a couple of pieces of information to identify yourself, specifically your name and email address. Please perform the requested operations and provide it with correct information; this will help me a lot when it comes time to grade your assignments.

For ease of use, you can give your personal remote repository a name, like this:

```console
cvw@student-cvw:lab01$ git remote add personal git@bitbucket.org:YOUR_BITBUCKET_USERNAME/netsec-YOUR_ODIN_ID.git
```

Then the push command is much simpler and easier to remember:

```console
cvw@student-cvw:lab01$ git push personal master
```

Challenge Problems
------------------
* [Problem 1](prob01.md)
* [Problem 2](prob02.md)
* [Problem 3](prob03.md)
* [Problem 4](prob04.md)
* [Problem 5](prob05.md)
* [Problem 6](prob06.md)
* [Problem 7](prob07.md)
* [Problem 8](prob08.md)
* [Problem 9](prob09.md)
* [Problem 10](prob10.md)


Did you remember to turn in your solutions?
-------------------------------------------
Did you get all the flags? Nicely done! Don't forget to turn in all of your code, flags, and answers to the in-line questions above.

The required series of steps is **git add**, then **git commit**, and finally **git push personal master**.

To see what files you have modified but not committed, you can use **git status**.
