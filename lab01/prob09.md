Problem 9: Spear Phishing
=========================
Bob thinks that maybe he should get a new flag from Alice.  He has a sneaking suspicion that the old one may have been compromised somehow.  Alice is expecting his request for a new flag.  She suspects that Bob's mail server cannot be trusted anymore, so instead she is planning to send the flag directly to Bob over TCP.

Write a program that forges an email to Alice (alice@example.com) from Bob (bob@example.com) by connecting to Alice's SMTP server at 10.96.0.1 on port 1025.  The subject of your email should be "FLAG", and in the body it should say "GOTO (your IP) (your port)".  For example, if your VM has the IP address 10.96.0.133, then you might send a message with the body "GOTO 10.96.0.133 2500".  Alice will then connect via TCP to send you the new flag.

**HINT**: In order to capture this flag, you must actually be listening on the TCP port that you tell Alice to use!
