#!/usr/bin/python3

import socket

def get_flag():
    flag = "FLAG 0123456789"
    UDP_IP = '10.96.0.1' # Charles IP address. NOT WORKING
    # UDP_IP = '127.0.0.1' # My IP address.
    # UDP_IP = '0.0.0.0' # listen on all interfaces
    UDP_PORT = 1101
    # MESSAGE = "Hello, World!".encode() #encoding string to avoid "byte-like" error.

    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    # sock.bind(('', UDP_PORT))
    # sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

    while True:
        # print ("inside true")
        data, addr = sock.recvfrom(1024) #buffer size is 1024 bytes
        # print ("address is:\n", addr)
        print ("received message:", data)
        # print ("\naddress is:",addr)
        flag = data
        # FLAG 17b593yy8c66bed40fe4a3
        if flag != "FLAG 0123456789":
            break

    # flag = "FLAG 17b593yy8c66bed40fe4a3"
    return flag

if __name__ == "__main__":
    get_flag()

