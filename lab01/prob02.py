#!/usr/bin/python3

import socket

def get_flag():
    flag = "FLAG 0123456789"
    UDP_IP = '10.96.0.1'
    # UDP_IP = "0.0.0.0" # send to anything.
    UDP_PORT = 1102
    MESSAGE = "Hello, World!".encode()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))


    # while True:
        # data, addr = sock.recvfrom(1024) #buffer size is 1024 bytes.
        # print("received message:", data)
    # return flag

if __name__ == "__main__":
    # Your code goes here
    get_flag()
