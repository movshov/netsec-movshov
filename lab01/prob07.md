Problem 7: TCP Wild Goose Chase
===============================
Follow the example code at [python.org](https://wiki.python.org/moin/TcpCommunication) to write a TCP client.  Connect to IP address 10.96.0.1 on TCP port 1107 and request the flag.

The server will reply in one of two ways.  Either it will give you the flag, or it will point you to another server that can help you find the flag.  Follow the clues until you find the flag!
