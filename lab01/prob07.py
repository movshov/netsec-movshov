#!/usr/bin/python3

import socket

def get_flag():
    flag = "FLAG 0123456789"
    TCP_IP = '10.96.0.1'
    TCP_PORT = 1107
    BUFFER_SIZE = 1024
    INITIAL = "".encode()
    MESSAGE = "GET FLAG\r\n".encode()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    s.send(INITIAL)
    data = s.recv(BUFFER_SIZE)
    s.close()
    print("flag is: {}\n".format(data))

    return flag

if __name__ == "__main__":
    # Your code goes here
    get_flag()
