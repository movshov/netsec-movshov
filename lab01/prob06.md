Problem 6: TCP Client with Challenge Response
=============================================
Follow the example code at [python.org](https://wiki.python.org/moin/TcpCommunication) to write a TCP client. Connect to IP address 10.96.0.1 on TCP port 1106 and the server will send you a math puzzle. Send the correct answer back, and then you may demand the flag using the Flag Retrieval Protocol from [Problem 4](prob04.md).

Turn in your code as [prob06.py](prob06.py) and your flag in [flag06.txt](flag06.txt) in git.
