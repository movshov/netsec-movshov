Problem 4: A Simple TCP Client
==============================
Follow the example code at [python.org](https://wiki.python.org/moin/TcpCommunication) to write a TCP client. Connect to IP address 10.96.0.1 on TCP port 1104.  And request the flag.


The "Flag Retrieval" Protocol
-----------------------------
Many of the later problems in the course will use the same simple protocol to request and retrieve the flag.  This is the first problem where you'll use that protocol.  It's *very* simple.  To request the flag from the server, simply send it the message 

```
GET FLAG\r\n
```
 -- that's the words "get flag" in all caps, followed by a carriage return and a line feed.
 
If your request is successful, the server will send you the flag, like this:

```
FLAG 0123456789\r\n
```

That is, the word "flag" in all caps, followed by a space, followed by the actual flag, followed by a carriage return and a line feed.



