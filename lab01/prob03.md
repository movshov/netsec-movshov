Problem 3: A Simple TCP Server
==============================

Follow the example code at [python.org](https://wiki.python.org/moin/TcpCommunication) to write a TCP server. Listen on TCP port 1103 for a message containing the flag.

Turn in your code as [prob03.py](prob03.py) and your flag in [flag03.txt](flag03.txt) in git.


Question 3
----------
Q. Would your alternate approach for capturing the flag in [Problem 1](prob01.md) still work here? Why or why not?

Submit your answer as [answer03.txt](answer03.txt) in git.

