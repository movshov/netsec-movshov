Problem 8: IMAP Client
======================
Alice sent the flag to Bob in an email. Bob's IMAP server is at 10.96.0.1 on port 1430.  Unfortunately for Bob, he didn't pick a very good *password*. In fact, the **password** that Bob chose for his *password* is about the worst **password** that he might have chosen for his password.

**HINT**: The Subject line of the email is "FLAG".

Your task is to write a program that logs in to Bob's email using his password and retrieves the flag. Documentation for the IMAPv4 protocol can be found in [RFC 3501](http://tools.ietf.org/html/rfc3501). IMAP stores messages in a very old format based on [RFC 2822](http://tools.ietf.org/html/rfc2822). Python provides some nice library functions for handling this format, in the [email](https://docs.python.org/3.7/library/email.html) package.

**HINT**: Bob's password is a VERY obvious *password*.

Turn in your flag-stealing program as [prob08.py](prob08.py) and your flag as [flag08.txt](flag08.txt) in git.
