#!/usr/bin/python3

import socket
import sys

def get_flag():
    flag = "FLAG 0123456789"
    #TCP_IP = '0.0.0.0'
    TCP_IP = '10.96.0.1'
    TCP_PORT = 1106
    BUFFER_SIZE = 1024
    INITIAL = "".encode()
    MESSAGE = "GET FLAG\r\n".encode()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    s.send(INITIAL)
    problem = s.recv(BUFFER_SIZE)
    print("{}\n".format(problem))
    math = input("Solve the following math problem: ")
    s.send(math.encode())
    check = s.recv(BUFFER_SIZE)
    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    s.close()
    print("flag is: {}\n".format(data))

    return flag

if __name__ == "__main__":
    # Your code goes here
    get_flag()
