Problem 2: A Simple UDP Client
==============================

Follow the example code at [python.org](https://wiki.python.org/moin/UdpCommunication) to write a UDP client. Send a packet to IP address 10.96.0.1 on UDP port 1102 and listen on the same UDP port (1102) for a response packet containing the flag.

Turn in your solutions as [prob02.py](prob02.py) and [flag02.txt](flag02.txt). 
