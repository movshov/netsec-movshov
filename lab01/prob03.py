#!/usr/bin/python3

import socket

def get_flag():
    flag = "FLAG 0123456789"
    # TCP_IP = '127.0.0.1'
    # TCP_IP = '10.96.0.1'
    TCP_IP = '0.0.0.0'
    TCP_PORT = 1103
    BUFFER_SIZE = 1024  # need to increase buffer from base of 20. 

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)

    conn, addr = s.accept()
    print('Connection address:', addr)
    while 1:
        data = conn.recv(BUFFER_SIZE)
        if not data: break
        print("recieved data:", data)
        conn.send(data) # echo
        # flag = data #original version
        flag = data.rstrip()
        flag = flag.decode('utf-8')
    conn.close()
    return flag

if __name__ == "__main__":
    # Your code goes here
    # get_flag() #original
    flag = get_flag()
    print ("flag is: {}\n".format(flag))

