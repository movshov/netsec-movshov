# Public repo for Network Security
This public repository contains bare-bones skeleton code that you can use as the basis for each programming lab.

Clone this repo to get started.

The descriptions for each programming assignment are here:

* [Lab 01: The TCP/IP Treasure Hunt](lab01/README.md)
* [Lab 02: Intro to Symmetric Cryptography](lab02/README.md)
* [Lab 03: Public Key Crypto](lab03/README.md)
