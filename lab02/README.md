Lab 2: Intro to Symmetric Cryptography
======================================

In this lab, we learn to encrypt and decrypt messages using an off-the-shelf crypto API.

Challenge Problems
------------------
* [Problem 1: Encoding Binary Data (Part 1)](prob01.md)
* [Problem 2: Encoding Binary Data (Part 2)](prob02.md)
* [Problem 3: Simple Decryption (ECB)](prob03.md)
* [Problem 4: Simple Decryption (CBC)](prob04.md)
* [Problem 5: Verifying a Hash](prob05.md)
* [Problem 6: Verifying a MAC](prob06.md)
* [Problem 7: Bad Encryption Password](prob07.md)
