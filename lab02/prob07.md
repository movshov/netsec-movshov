Problem 7: Bad Encryption Password
==================================
Connect to the server at 10.96.0.1 on port 2007 and request the flag as in previous problems.

The server's response will be of the form "FLAG flag\r\n", encrypted with [AES-128](https://pycryptodome.readthedocs.io/en/latest/src/cipher/aes.html) in [CBC mode](https://pycryptodome.readthedocs.io/en/latest/src/cipher/classic.html#cbc-mode) and transmitted as **raw bytes**. The encryption key was generated in a *very insecure way*, by taking the [MD5 hash](https://pycryptodome.readthedocs.io/en/latest/src/hash/md5.html) of a dictionary word.

Find the password that generates the secret key, use it to decrypt the server's response, and extract the flag.

**HINT 1**: The Ubuntu Linux distribution provides a list of thousands of American English words in the package *wamerican*.  You can install it like this:
```console
sudo apt install wamerican
```
Then you can find the words list in the file **/usr/share/dict/words**.

**HINT 2**: The password was transformed into all lowercase before it was hashed to generate the key.

**HINT 3**: Once you have received the encrypted message, you can simply try every word in the dictionary.  For each word, compute the key and try to decrypt with it.  If the resulting plaintext starts with "FLAG ", you've probably found the right one!
