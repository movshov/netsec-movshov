#!/usr/bin/python3

import socket
import binascii
from Crypto.Cipher import AES
from base64 import b64decode
#from Crypto.Util.Padding import unpad  #NEED TO FIX SOMEHOW!

#
# get_flag() - This is the function that will be called by the automated
#              grading script.  Upon each invocation, it should dynamically
#              interact with the lab environment to capture and return the
#              flag.  So, for example, if I run this from my "student" VM,
#              it should return my flag for the given problem.  As another
#              example, any solution that simply hard-codes a flag found
#              through interactive exploration (eg using netcat/telnet)
#              should expect to fail the test suite and receive zero points
#              for the code portion of the grade.
def get_flag():
    flag = "FLAG ..."
    TCP_IP = '10.96.0.1'
    TCP_PORT = 2004
    BUFFER_SIZE = 1024
    MESSAGE = "GET FLAG\r\n".encode()
    key_string_hex = "20200204" * 4
    print("key_string_hex is: {}\n".format(key_string_hex))

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    print("raw data is: {}\n".format(data))
    s.close()

    key_as_bytes = binascii.unhexlify(key_string_hex)   #convert key_string_hex to raw bytes.
    print("key as raw bytes is: {}\n".format(key_as_bytes))
    iv = data[:AES.block_size] #first block of data is the iv. 
    cipher = AES.new(key_as_bytes, AES.MODE_CBC, iv)
    flag = unpad(cipher.decrypt(data[AES.block_size:]), AES.block_size)
    print("flag is: {}\n".format(flag))
    return flag


if __name__ == "__main__":
  get_flag()

