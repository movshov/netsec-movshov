Problem 2: Encoding Binary Data (Part 2)
========================================

Connect to the server at 10.96.0.1 on port 2002 and request the flag as in [Lab 01](../lab01/README.md).

Unlike in Lab 01, where the flags were always ASCII text, this flag will be sent to you as **raw binary bytes**. The format of the server's response message will be
```
FLAG length flag\r\n
```
where *length* is the length (in bytes) of *flag*, written as a standard decimal integer.

Before you return the flag, transform it into a nice human-readable format in the ASCII character set, using **base64 encoding**. You can use Python's [base64](https://docs.python.org/3.7/library/base64.html) module or a similar facility to do this. 
