#!/usr/bin/python3

import socket
import binascii
from Crypto.Hash import HMAC, SHA

#
# get_flag() - This is the function that will be called by the automated
#              grading script.  Upon each invocation, it should dynamically
#              interact with the lab environment to capture and return the
#              flag.  So, for example, if I run this from my "student" VM,
#              it should return my flag for the given problem.  As another
#              example, any solution that simply hard-codes a flag found
#              through interactive exploration (eg using netcat/telnet)
#              should expect to fail the test suite and receive zero points
#              for the code portion of the grade.

def get_flag():
    flag = "FLAG ..."
    TCP_IP = '10.96.0.1'
    TCP_PORT = 2006
    BUFFER_SIZE = 1024
    MESSAGE = "GET FLAG\r\n".encode()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    print("raw data is: {}\n".format(data))
    s.close()
    
    flag = data[:21]
    mac = data[22:]
    print("flag is: {}\n".format(flag))
    print("mac is: {}\n".format(mac))

    mac_secret_key = b'20200206202002062020020620200206'
    h = HMAC.new(mac_secret_key, digestmod=SHA)
    #h = HMAC.new(mac_secret_key, digestmod=SHA1)
    h.update(flag)
    flag_test = h.hexdigest()
    print(b"flag_test is:",flag_test)


    return flag



if __name__ == "__main__":
  get_flag()

