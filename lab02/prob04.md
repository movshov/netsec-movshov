Problem 4: Simple Decryption (CBC)
==================================

Connect to the server at 10.96.0.1 on port 2004 and request the flag as in previous problems.

The server's response will be encrypted with [AES](https://pycryptodome.readthedocs.io/en/latest/src/cipher/aes.html) in [CBC mode](https://pycryptodome.readthedocs.io/en/latest/src/cipher/classic.html#cbc-mode) and transmitted as **raw bytes**.

The secret key, as a hexadecimal string, is 20 20 02 04 20 20 02 04 20 20 02 04 20 20 02 04. Use [binascii](https://docs.python.org/3.7/library/binascii.html) to convert these hex digits to **16 raw bytes** in order to use them as an AES-128 key.

Decrypt the message and recover the flag.

**HINT**: Remember that when we use CBC mode, the first block of data is the **initialization vector** (IV), not ciphertext!

