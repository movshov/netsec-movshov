Problem 3: Simple Decryption (ECB)
==================================
Connect to the server at 10.96.0.1 on port 2003 and request the flag as in previous problems.

The server's response will be encrypted with [AES](https://pycryptodome.readthedocs.io/en/latest/src/cipher/aes.html) in [ECB mode](https://pycryptodome.readthedocs.io/en/latest/src/cipher/classic.html#ecb-mode) and transmitted as **raw bytes**.  You must decrypt the flag before you return it.

The 128-bit secret key, as a hexadecimal string, is 20 20 02 03 20 20 02 03 20 20 02 03 20 20 02 03.

The pattern that we use to determine symmetric keys in this lab is: 2 bytes for the current year (eg 20 20), one byte for the lab number (02), and one byte for the problem number (03).  That gives us a total of 4 bytes, so to get a 128-bit (16-byte) key, we simply repeat the pattern 4 times.  In Python, you can also write it like this to reduce the chance of a typo:

```python
key_string_hex = "20200203" * 4
```


Use [binascii](https://docs.python.org/3.7/library/binascii.html) or something similar to convert these hex digits to **16 raw bytes** in order to use them as an AES-128 key.

Then, use AES to decrypt the message and recover the flag.

**HINT**: [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/) provides a nice Python interface to the relevant AES routines -- see [Cryptodome.Cipher.AES](https://pycryptodome.readthedocs.io/en/latest/src/cipher/aes.html).

