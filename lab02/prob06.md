Problem 6: Verifying a MAC
==========================

Connect to the server at 10.96.0.1 on port 2006 and request the flag as in previous problems.

The server's response will be provided in plain text, and it will include a **message authentication code** (MAC) to help you verify the integrity of the message.

The message format for the server's response is:
```
FLAG flag mac\r\n
```
where *mac* = MAC("FLAG *flag*"). The MAC is computed using **HMAC-SHA1** -- that is, with [HMAC](https://pycryptodome.readthedocs.io/en/latest/src/hash/hmac.html) using [SHA-1](https://pycryptodome.readthedocs.io/en/latest/src/hash/sha1.html) as the hash.  The MAC "digest" is encoded in the message as **hexadecimal ASCII** characters.

The secret key for the MAC, as a hexadecimal string, is 20 20 02 06 20 20 02 06 20 20 02 06 20 20 02 06 20 20 02 06.

**NOTE**: Like in [Problem 5](prob05.md), this server often returns garbled flags and/or incorrect MACs.  Be sure to verify that MAC, or you might return the wrong flag!

