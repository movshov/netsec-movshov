#!/usr/bin/python3

import socket
import binascii
#
# get_flag() - This is the function that will be called by the automated
#              grading script.  Upon each invocation, it should dynamically
#              interact with the lab environment to capture and return the
#              flag.  So, for example, if I run this from my "student" VM,
#              it should return my flag for the given problem.  As another
#              example, any solution that simply hard-codes a flag found
#              through interactive exploration (eg using netcat/telnet)
#              should expect to fail the test suite and receive zero points
#              for the code portion of the grade.

def get_flag():
    flag = "FLAG ..."
    TCP_IP = '10.96.0.1'
    TCP_PORT = 2001
    BUFFER_SIZE = 1024
    MESSAGE = b"GET FLAG\r\n"

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    s.send(MESSAGE)
    data = s.recv(BUFFER_SIZE)
    print("raw data is: {}\n".format(data))
    s.close()
    old = data
    data = data[7:] # get rid of 'Flag # '
    print("raw data after strip: {}\n".format(data))
    flag = b"FLAG " + binascii.hexlify(data)
    #flag = b"FLAG " + binascii.b2a_hex(data)
    #flag = b"FLAG " + binascii.b2a_uu(data)
    #flag = "FLAG " + data.hex()
    old = b"FLAG " + binascii.hexlify(old)
    print("old is: {}\n".format(old))
    print("flag is: {}\n".format(flag))
    return flag

if __name__ == "__main__":
  get_flag()


