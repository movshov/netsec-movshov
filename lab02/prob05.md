Problem 5: Verifying a Hash
===========================

Connect to the server at 10.96.0.1 on port 2005 and request the flag as in previous problems.

The server's response will be provided in **plain text**, and it will include a hash to let you verify the integrity of the message.

The message format for the server's response is:
```
FLAG flag hash\r\n
```
where *hash* = H("FLAG *flag*").  The hash is computed using [SHA-256](https://pycryptodome.readthedocs.io/en/latest/src/hash/sha256.html) and the hash "digest" is encoded in the message as **hexadecimal ASCII characters**. (So, for example, the server might have computed it using the [.hexdigest()](https://pycryptodome.readthedocs.io/en/latest/src/hash/sha256.html#Crypto.Hash.SHA256.SHA256Hash.hexdigest) method.)

**NOTE**: This server can be unreliable!  It often returns garbled flags and/or incorrect hashes.  Be sure to verify that hash, or you might return the wrong flag!

